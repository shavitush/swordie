# San Commerci | Used to complete a Quest:  [Commerci Republic] Ciao, Until Next Time
def init():
    if sm.hasQuest(17614): # [Commerci Republic] Ciao, Until Next Time
        sm.completeQuest(17614)
    sm.dispose()